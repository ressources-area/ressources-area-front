# Ressources-Area - RA-Front (Angular)
## Description
Ra-Front est un client web développé avec Angular.
## Développement
Le client utilise Angular 11.2.5.
Développé sur Intellij et VSCode.

## Installation et Run
1) Aller dans le dossier /ressource-area et lancer une invite de commande puis
rentrer "ng serve". Cela lancera le client. Lancer d'abord la base de données 
   et les services.

## Owner
BRICHET Benoît - brichet.b53@gmail.com

Ressources-Area a été développé dans le cadre d'un projet final
d'une formation de développeur Java via OpenClassrooms

