import {Component, OnDestroy, OnInit} from '@angular/core';
import {AgentModel} from '../../models/Agent.model';
import {CompetenceAgentModel} from '../../models/CompetenceAgent.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AgentService} from '../../services/agent.service';

@Component({
  selector: 'app-agent-info',
  templateUrl: './agent-info.component.html',
  styleUrls: ['./agent-info.component.scss']
})
export class AgentInfoComponent implements OnInit {

  agent!: AgentModel;
  // @ts-ignore
  competenceAgent: CompetenceAgentModel [] = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private agentService: AgentService) {
  }

  ngOnInit(): void {
    let matricule = this.route.snapshot.params.matricule;
    this.agentService.getProfilByMatriculeAgent(matricule).subscribe(
      (agent) => {
        this.agent = agent;
        this.agentService.getCompetenceAgentByMatricule(this.agent.matricule).subscribe(
          (data: CompetenceAgentModel[]) => {
            this.competenceAgent = data;
          }
        );
      }
    );
  }

  goBack(): void {
    this.router.navigate(['agent/list-agent']);
  }

  goDeleteAgent(agent: AgentModel): void {
    this.agentService.deleteAgent(agent);
    let link = ['/agent/profil'];
    this.router.navigate(link);
  }

  goEditRoleForAdmin(agent: AgentModel){
    this.agentService.updateRoleAgent(agent);
    let link = ['/agent/agent-info/'+ agent.matricule];
    this.router.navigate(link);
  }


}
