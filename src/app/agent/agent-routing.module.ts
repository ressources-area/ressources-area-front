import {RouterModule, Routes} from '@angular/router';
import {SignupComponent} from '../auth/signup/signup.component';
import {SigninComponent} from '../auth/signin/signin.component';
import {ListAgentComponent} from './list-agent/list-agent.component';
import {ProfilComponent} from './profil/profil.component';
import {NgModule} from '@angular/core';
import {EditProfilComponent} from './edit/editProfil.component';
import {EditCompetenceComponent} from './edit/editCompetence.component';
import {GuardService} from '../services/guard.service';
import {AdminGuardService} from '../services/admin-guard.service';
import {AgentInfoComponent} from './agent-info/agent-info.component';
import {EditPasswordComponent} from './edit/edit-password.component';
import {AgentGuardService} from '../services/agent-guard.service';

const agentRoutes: Routes = [
  {
    path: 'auth',
    children : [
      { path: 'signup', component: SignupComponent},
      { path: 'signin', component: SigninComponent},
    ]
  },
  {
    path: 'agent',
    canActivate: [AgentGuardService],
    children: [
      { path: 'editProfil/:matricule', component: EditProfilComponent},
      { path: 'editCompetence/:matricule', component: EditCompetenceComponent},
      { path: 'editPassword/:matricule', component: EditPasswordComponent}
    ]
  },
  {
    path: 'agent',
    canActivate: [GuardService],
    children: [
      { path: 'profil', component: ProfilComponent},
    ]
  },
  {
    path: 'agent',
    canActivate: [AdminGuardService],
    children: [
      { path: 'list-agent', component: ListAgentComponent},
      { path: 'agent-info/:matricule', component: AgentInfoComponent}
      ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(agentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AgentRoutingModule {

}

