import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SigninComponent} from '../auth/signin/signin.component';
import {SignupComponent} from '../auth/signup/signup.component';
import {ProfilComponent} from './profil/profil.component';
import {ListAgentComponent} from './list-agent/list-agent.component';
import {AgentRoutingModule} from './agent-routing.module';
import {EditProfilComponent} from './edit/editProfil.component';
import {EditFormComponent} from './edit/edit-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditCompetenceComponent} from './edit/editCompetence.component';
import {EditCompetenceFormComponent} from './edit/editCompetence-form.component';
import {AgentService} from '../services/agent.service';
import {GuardService} from '../services/guard.service';
import {AuthService} from '../services/auth.service';
import {AdminGuardService} from '../services/admin-guard.service';
import {AgentGuardService} from '../services/agent-guard.service';
import {TreeTableModule} from 'primeng/treetable';
import {MatTableModule} from '@angular/material/table';
import {SearchAgentComponent} from './list-agent/search-agent.component';
import {AgentInfoComponent} from './agent-info/agent-info.component';
import {EditPasswordComponent} from './edit/edit-password.component';
import {LoaderComponent} from '../loader.component';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AgentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TreeTableModule,
    MatTableModule,
  ],
  declarations: [
    SigninComponent,
    LoaderComponent,
    SignupComponent,
    ProfilComponent,
    ListAgentComponent,
    EditProfilComponent,
    EditCompetenceComponent,
    EditFormComponent,
    SearchAgentComponent,
    EditPasswordComponent,
    EditCompetenceFormComponent,
    AgentInfoComponent,
    AgentInfoComponent,
  ],
  providers: [
    AgentService,
    GuardService,
    AuthService,
    AdminGuardService,
    AgentGuardService,
  ]
})
export class AgentModule { }
