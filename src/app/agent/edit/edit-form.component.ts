import {Component, Input, OnInit} from '@angular/core';
import {AgentModel} from '../../models/Agent.model';
import {Router} from '@angular/router';
import {AgentService} from '../../services/agent.service';
import {GradeModel} from '../../models/Grade.model';
import {CentreModel} from '../../models/Centre.model';
import {AuthService} from '../../services/auth.service';

@Component({
  selector:'edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditFormComponent implements OnInit {

  // @ts-ignore
  @Input() agent: AgentModel;
  gradeList!: GradeModel[];
  centreList!: CentreModel[];

  constructor(private router: Router,
              private agentService: AgentService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.agentService.getProfilByAgentConnected().subscribe(
      (data) => {
        this.agent = data;
      }
    );
    this.agentService.getAllGrade().subscribe(
      (data: GradeModel[]) => {
        this.gradeList = data;
      }
    );
    this.agentService.getAllCentre().subscribe(
      (data: CentreModel[]) => {
        this.centreList = data;
      }
    );
  }


  // Méthode appelée lorsque le formulaire est soumis.
  onSubmit(): void {

  this.agentService.updateAgent(this.agent)
    .subscribe(
      () => {
      this.goBack();
    },
      () => {
      this.goBack();
    }
  )
  }

  goBack(): void {
    let link = ['agent/profil'];
    this.router.navigate(link);
  }

}
