import {Component, Input, OnInit} from '@angular/core';
import {AgentModel} from '../../models/Agent.model';
import {Router} from '@angular/router';
import {AgentService} from '../../services/agent.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditPasswordComponent implements OnInit {

  @Input() agent!: AgentModel;
  registrerForm!: FormGroup;
  submitted = false;

  constructor(private router: Router,
              private agentService: AgentService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.agentService.getProfilByAgentConnected().subscribe(
      (data) => {
        this.agent! = data;
      }
    );

    this.registrerForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmationNewPassword: ['', Validators.required],
    }, {
      validator: this.checkPasswords('oldPassword', 'newPassword', 'confirmationNewPassword'),
    });
  }

  checkPasswords(controlOldPassword: string, controlPassword: string, matchingControlPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlPassword];
      const matchingControl = formGroup.controls[matchingControlPassword];
      const oldControl = formGroup.controls[controlOldPassword];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
        return;
      }
        if (control.value !== matchingControl.value) {
          matchingControl.setErrors({confirmedValidator: true});
        } else {
          matchingControl.setErrors(null);
        }
    };
  }

  get f() {
    return this.registrerForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.registrerForm.invalid) {
      console.log('Invalid');
      return;
    }

    this.agentService.updatePassword(this.agent, this.registrerForm.controls['oldPassword'].value,
      this.registrerForm.controls['newPassword'].value);


    let link = ['agent/profil'];
    this.router.navigate(link);
  }
}
