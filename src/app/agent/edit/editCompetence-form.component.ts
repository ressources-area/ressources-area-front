import {Component, Input, OnInit} from '@angular/core';
import {CompetenceAgentModel} from '../../models/CompetenceAgent.model';
import {AgentModel} from '../../models/Agent.model';
import {AgentService} from '../../services/agent.service';
import {Router} from '@angular/router';
import {CompetenceModel} from '../../models/Competence.model';
import {first} from 'rxjs/operators';

@Component({
  selector: 'editCompetence-form',
  templateUrl: 'editCompetence.component.html',
  styleUrls: ['edit.component.scss']
})

export class EditCompetenceFormComponent implements OnInit {

  @Input()
  competenceAgentList!: CompetenceAgentModel[];
  @Input() agent!: AgentModel;
  competenceList!: CompetenceModel[];

  constructor(private router: Router,
              private agentService: AgentService) {
  }

  ngOnInit() {

    this.agentService.getProfilByAgentConnected().subscribe(
      (data) => {
        this.agent = data;
        this.agentService.getCompetenceAgentByMatricule(this.agent.matricule).subscribe(
          (data: CompetenceAgentModel[]) => {
            this.competenceAgentList = data;
          }
        );
        this.agentService.getAllCompetence().subscribe(
          (data: CompetenceModel[]) => {
            this.competenceList = data;
          }
        );
      }
    );
  }

  // Méthode appelée lorsque l'utilisateur ajoute ou retire une compétence à l'agent en cours d'édition.
  selectCompetence($event: any, competenceAdd: CompetenceModel): void {
    let checked = $event.target.checked;
    let competenceAgent = new CompetenceAgentModel();
      competenceAgent.competence = competenceAdd;
      competenceAgent.agent = this.agent;
      if (checked) {
        this.competenceAgentList.push(competenceAgent);
      } else {
        for (let i = 0; i < this.competenceAgentList.length; i++) {
          competenceAgent = this.competenceAgentList[i];
          if (competenceAgent.competence.intitule.match(competenceAdd.intitule)){
            this.competenceAgentList.splice(i,1);
          }
        }
      }
  }

  hasCompetence(competenceValid: CompetenceModel): boolean {
    var competenceAgent = new CompetenceAgentModel();
    for (let i = 0; i < this.competenceAgentList.length; i++) {
      competenceAgent = this.competenceAgentList[i];
      if (competenceAgent.competence.intitule.match(competenceValid.intitule)) {
        return true;
      }
    }
    return false;
  }

  changeFormateur($event: any,competenceAgent: CompetenceAgentModel): void {
    let checked = $event.target.checked;
    if (checked){
      competenceAgent.formateur = true;
    } else {
      competenceAgent.formateur = false;
    }
  }

  hasFormateur(competenceAgent: CompetenceAgentModel): boolean {
    if (competenceAgent.formateur){
      return true;
    } return false;
  }

  onSubmit(): void {

    this.agentService.updateCompetenceAgent(this.competenceAgentList, this.agent)
      .pipe(first())
      .subscribe(
        data => {
          let link = ['agent/profil'];
          this.router.navigate(link);
        },
        error => {
          let link = ['/agent/editCompetence/',this.agent.matricule];
          this.router.navigate(link);
        }
      )
  }

}
