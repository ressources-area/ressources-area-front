import {Component, OnInit} from '@angular/core';
import {AgentModel} from '../../models/Agent.model';
import {ActivatedRoute} from '@angular/router';
import {AgentService} from '../../services/agent.service';
import {CompetenceAgentModel} from '../../models/CompetenceAgent.model';

@Component({
  selector: 'edit-agent',
  template: `
    <edit-form [agent]="agent"></edit-form>
  `,
  styleUrls: ['./edit.component.scss']
})
export class EditProfilComponent implements OnInit {

  agent!: AgentModel;
  competenceAgent!: CompetenceAgentModel[];

  constructor(private route: ActivatedRoute,
              private agentService: AgentService) { }

  ngOnInit(): void {

  }

}
