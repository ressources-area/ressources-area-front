import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AgentService} from '../../services/agent.service';
import {AgentModel} from '../../models/Agent.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-agent',
  templateUrl: './list-agent.component.html',
  styleUrls: ['./list-agent.component.scss'],
  providers: [AgentService]
})
export class ListAgentComponent implements OnInit, OnDestroy {

  agentList!: AgentModel[];
  agentListSubscription!: Subscription;

  isShown: boolean = false;


  constructor(private router: Router,
              private agentService: AgentService) {
  }

  ngOnInit(): void {

    this.agentListSubscription = this.agentService.agentListSubject.subscribe(
      agents => this.agentList = agents
    );

    this.agentService.getAgentList();
  }

  toggleShow(){
  this.isShown = ! this.isShown;
  }

  selectAgent(agent: AgentModel): void {
    let link = ['/agent/agent-info/'+ agent.matricule];
    this.router.navigate(link);
  }

  itsMe(agent: AgentModel): boolean{
    if (agent.matricule === localStorage.getItem('currentMatricule')){
      return true
    }
      return false;
  }

  ngOnDestroy():void {
    this.agentListSubscription.unsubscribe();
  }
}
