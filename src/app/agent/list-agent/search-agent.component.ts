import {Component, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SummaryAgentModel} from '../../models/SummaryAgent.model';
import {AgentService} from '../../services/agent.service';
import {Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {AgentModel} from '../../models/Agent.model';

@Component({
  selector:'agent-search',
  styleUrls: ['./list-agent.component.scss'],
  templateUrl: './search-agent.component.html'
})

export class SearchAgentComponent implements OnInit{

  private searchTerms = new Subject<string>();
  // @ts-ignore
  agent$: Observable<SummaryAgentModel[]>;

  constructor(private agentService: AgentService,
              private router: Router) {
  }

  search(term:string){
    this.searchTerms.next(term);
  }

  ngOnInit(): void {

    this.agent$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term:string) => this.agentService.searchAgent(term)),
    );
  }

  selectAgent(matricule: string) {
    let link = ['/agent/agent-info/'+ matricule];
    this.router.navigate(link);
  }

}
