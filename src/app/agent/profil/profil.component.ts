import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AgentModel} from '../../models/Agent.model';
import {CompetenceAgentModel} from '../../models/CompetenceAgent.model';
import {AgentService} from '../../services/agent.service';
import {TokenService} from '../../services/token.service';
import {DOCUMENT} from '@angular/common';
import { StageModel } from 'src/app/models/Stage.model';
import { StageService } from 'src/app/services/stage.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
  providers: [AgentService,TokenService]
})
export class ProfilComponent implements OnInit {

  agent!: AgentModel;
  // @ts-ignore
  competenceAgent!: CompetenceAgentModel [];
  stageList!: StageModel[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private agentService: AgentService,
              private stageService: StageService,
              @Inject(DOCUMENT) private _documents: Document) { }

  ngOnInit(): void {
    this.agentService.getProfilByAgentConnected().subscribe(
      (data) => {
        this.agent! = data;
        this.agentService.getCompetenceAgentByMatricule(this.agent.matricule).subscribe(
          (data: CompetenceAgentModel[]) => {
            this.competenceAgent! = data;
          }
        );
        this.stageService.getStageByMatriculeAgent(this.agent.matricule).subscribe(
          (data: StageModel[]) => {
            this.stageList = data;
          }
        )
      }
    );
  }


  goEditProfil(agent: AgentModel): void {
    let link = ['/agent/editProfil/',agent.matricule];
    this.router.navigate(link);
  }

  goEditCompetence(competenceAgent: CompetenceAgentModel[], agent: AgentModel): void {
    let link = ['/agent/editCompetence/',agent.matricule];
    this.router.navigate(link);
  }

  goEditPassword(agentModel: AgentModel): void {
    let link = ['/agent/editPassword/',agentModel.matricule];
    this.router.navigate(link);
  }

  selectStage(idStage: number){
    let link = ['/stage/info-stage/'+idStage];
    this.router.navigate(link);
  }

}
