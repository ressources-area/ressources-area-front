import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {AuthService} from './services/auth.service';
import {GuardService} from './services/guard.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TokenService} from './services/token.service';
import {CookieModule, CookieOptionsProvider, CookieService} from 'ngx-cookie';
import {PageNotFoundComponent} from './page-not-found.component';
import {NodeService} from './services/node-service.service';
import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import {RessourceModule} from './ressources/ressource.module';
import {AgentModule} from './agent/agent.module';
import {CommonModule} from '@angular/common';
import { StageModule } from './stage/stage.module';
import { BackgroundComponent } from './background/background.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent,
    BackgroundComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RessourceModule,
    AgentModule,
    FormsModule,
    StageModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    CookieModule.forRoot()
  ],
  providers: [
    AuthService,
    GuardService,
    TokenService,
    CookieService,
    NodeService,
    CookieOptionsProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
