import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {AlerteService} from '../../services/alerte.service';
import {first} from 'rxjs/operators';
import {AuthModel} from '../../models/Auth.model';
import {AgentService} from '../../services/agent.service';
import {TokenService} from '../../services/token.service';
import {DOCUMENT} from '@angular/common';
import {ErrorService} from '../../services/error.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  authModel!: AuthModel;
  message = '';

  constructor(private formBuild: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService,
              private tokenService: TokenService,
              private agentService: AgentService,
              private alerteService: AlerteService,
              @Inject(DOCUMENT) private _documents: Document) {
  }

  ngOnInit(): void {
    this.loginForm= this.formBuild.group({
      matricule: ['', Validators.required],
      password: ['', Validators.required]
    });
    if (localStorage.getItem('role')){
      this.router.navigate(['/agent/profil']);
    }
  }

  get f() { return this.loginForm.controls; }

  onSubmit(){
    this.submitted = true;

    if(this.loginForm.invalid){
      return;
    }

    this.loading= true;
    this.authModel = this.loginForm.value;
    this.authService.signIn(this.authModel)
      .pipe(first())
      .subscribe(
      () => {
        let redirect =
          this.authService.redirectUrl ? this.authService.redirectUrl : '/agent/profil';
        this.router.navigate([redirect]);
      },
      error => {
        this.message = "Le matricule ou le mot de passe n'est pas correct !";
        window.location.reload();
        this.router.navigate(['/auth/signin']);
      }
    );
  }
}
