import {Component, OnInit} from '@angular/core';
import {Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {AgentService} from '../../services/agent.service';
import {AlerteService} from '../../services/alerte.service';
import {first} from 'rxjs/operators';
import {GradeModel} from '../../models/Grade.model';
import {CentreModel} from '../../models/Centre.model';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  registrerForm!: FormGroup;
  // @ts-ignore
  gradeList!: GradeModel[];
  // @ts-ignore
  centreList!: CentreModel[];
  loading =false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private agentService: AgentService,
    private alerteService: AlerteService
  ) {
    if (this.authService.currentAgentValue){
      this.router.navigate(['/'])
    }
  }

  ngOnInit(): void {

    this.agentService.getAllCentre().subscribe(
      (data: CentreModel[]) => {
        this.centreList = data;
      }
    )

    this.agentService.getAllGrade().subscribe(
      (data: GradeModel[]) => {
        this.gradeList = data;
      }
    )

    this.registrerForm = this.formBuilder.group({
      prenom:['', Validators.required],
      nom:['', Validators.required],
      matricule:['', Validators.required],
      password:['', [Validators.required, Validators.minLength(6)]],
      confirmationPassword:['', Validators.required],
      centre:['', Validators.required],
      grade:['', Validators.required]
    }, {
      validators: this.checkMatricule('matricule','password', 'confirmationPassword'),
    })
    
  }

  checkMatricule(validateMatricule: string, controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const matricule = formGroup.controls[validateMatricule];
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matricule.errors && !matricule.errors.confirmedMatricule) {
        return;
      }

      if(matchingControl.errors && !matchingControl.errors.confirmationPassword){
        return;
      }

      if (matricule.value.length !== 4 || isNaN(matricule.value)) {
        matricule.setErrors({confirmedMatricule: true});
      } else {
        matricule.setErrors(null);
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }


  get f(){return this.registrerForm.controls;}

  onSubmit(){
    this.submitted = true

    if (this.registrerForm.invalid){
      console.log('Invalid')
      return;
    }

    this.authService.registrer(this.registrerForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alerteService.success('Inscription réussite', true)
          this.router.navigate(['/auth/signin']);
        },
        error => {
          this.alerteService.error(error);
          this.loading = false;
        }
      );
  }

}
