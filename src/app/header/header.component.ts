import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { AgentService } from '../services/agent.service';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Routes } from '@angular/router';
import { AgentModel } from '../models/Agent.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean = false;
  role!: string;
  agent!: AgentModel;
  mobileShow = false;

  constructor(private authService: AuthService,
              @Inject(DOCUMENT) private _documents: Document,
              private agentService: AgentService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

      this.authService.currentAuth.subscribe(isAuth => this.isAuth = isAuth);
      this.authService.currentRole.subscribe(role => this.role = role);
    
      if (localStorage.getItem("role")=== "ADMIN"){
        this.role! = "ADMIN";
        this.isAuth! = true;
      } else if(localStorage.getItem("role")=== "USER"){
        this.role! = "USER";
        this.isAuth! = true;
      } else {
        this.isAuth = false;
      }

  }


  deconnexion() {
    this.isAuth = false;
    this.authService.signout();
  }

  isShow(){
    if (this.mobileShow){
      this.mobileShow = false;
    } else {
      this.mobileShow = true;
    }
  }
}
