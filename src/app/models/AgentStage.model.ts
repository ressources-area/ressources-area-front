export class AgentStageModel {

  id!: number;
  status!: string;
  matriculeAgent!: string;
  nomAgent!: string;
  prenomAgent!: string;
  nomCentre!: string;
}
