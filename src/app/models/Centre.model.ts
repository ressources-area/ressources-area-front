export class CentreModel {

  id!: number;
  nom!: string;
  adresse!: string;
  codePostale!: string;
  ville!: string;

}
