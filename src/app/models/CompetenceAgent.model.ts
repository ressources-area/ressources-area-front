import {AgentModel} from './Agent.model';
import {CompetenceModel} from './Competence.model';

export class CompetenceAgentModel {

  id!: number;
  formateur!: boolean;
  agent!: AgentModel;
  competence!: CompetenceModel;

}
