import {GradeModel} from './Grade.model';
import {CentreModel} from './Centre.model';

export class AgentModel {

  id!: number;
  prenom!: string;
  nom!: string;
  matricule!: string;
  role!: string;
  password!: string;
  confirmationPassword!: string;
  mail!: string;
  grade!: GradeModel;
  centre!: CentreModel;

}
