import {NoteRessourceModel} from './NoteRessource.model';

export class RessourceModel {
  id!: number;
  titre!: string;
  lien!: string;
  module!: string;
  dateCreation!: Date;
  noteRessourceList!: NoteRessourceModel[];
}
