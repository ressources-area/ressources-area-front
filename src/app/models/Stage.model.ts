import {AgentStageModel} from './AgentStage.model';
import {NoteStageModel} from './NoteStage.model';

export class StageModel {

  constructor(public id :number,
              public intitule: string,
              public numero: string,
              public dateDebut: Date,
              public dateFin: Date,
              public lieu: string){ }
}
