import {GradeModel} from './Grade.model';
import {CentreModel} from './Centre.model';
import {CompetenceAgentModel} from './CompetenceAgent.model';

export class SummaryAgentModel{

  prenom!: string;
  nom!: string;
  matricule!: string;
  role!: string;
  grade!: GradeModel;
  centre!: CentreModel;
  competenceAgentList!: CompetenceAgentModel[];
}
