import {Component, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RessourceService} from '../../../services/ressource.service';
import { take } from 'rxjs-compat/operator/take';
import { of } from 'rxjs';
import {first} from 'rxjs/operators';
import { RessourceModel } from 'src/app/models/Ressource.model';

@Component({
  selector: 'app-add-ressource',
  templateUrl: './add-ressource.component.html',
  styleUrls: ['./add-ressource.component.scss']
})
export class AddRessourceComponent implements OnInit {

  selectedFiles!: FileList;
  currentFile!: File;
  progress = 0;
  message = '';
  test!: String;
  registrerRessourceForm!: FormGroup;
  submitted = false;

  fileInfos!: Observable<any>;

  constructor(private formBuilder: FormBuilder,
              private ressourceService: RessourceService,
              private router: Router) { }

  ngOnInit(): void {

    this.registrerRessourceForm = this.formBuilder.group({
      module:['',Validators.required],
      file:['',Validators.required]
    })
  }

  // @ts-ignore
  selectFile(event){
    this.selectedFiles = event.target.files;
  }

  get f(){
    return this.registrerRessourceForm.controls;
  }

  onSubmit() {

    this.submitted = true;

    // @ts-ignore
    this.currentFile = this.selectedFiles.item(0);

    this.ressourceService.upload(this.currentFile, this.registrerRessourceForm.value['module'])
      .pipe(first())
      .subscribe(
      event => {
        this.message = "Upload ok";
        this.ressourceService.getRessourceListByModule(this.registrerRessourceForm.value['module']);
        this.goBack(this.registrerRessourceForm.value['module']);
      },
      err => {
        this.progress = 0;
        this.message = "Erreur d'envoie";
        // @ts-ignore
        this.currentFile = undefined;
        this.router.navigate(['ressources/addRessource']);
      }
    );

  }

  goBack(module: string): void {
    let link = ['ressources/listRessource/'+module];
    this.router.navigate(link);
  }

}
