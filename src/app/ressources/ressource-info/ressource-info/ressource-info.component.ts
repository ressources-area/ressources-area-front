import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RessourceService } from '../../../services/ressource.service';
import { RessourceModel } from '../../../models/Ressource.model';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NoteRessourceModel } from 'src/app/models/NoteRessource.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ressource-info',
  templateUrl: './ressource-info.component.html',
  styleUrls: ['./ressource-info.component.scss']
})
export class RessourceInfoComponent implements OnInit, OnDestroy {

  isAuth!: Boolean;

  ressource!: RessourceModel;
  ressourceSubscription!: Subscription;

  message!: string;
  file!: File;
  @ViewChild('viewer')
  viewerRef!: ElementRef;
  URLFile!: SafeResourceUrl;
  role!: string;
  noteRessourceForm!: FormGroup;
  noteRessource!: NoteRessourceModel;
  isShow = false;
  submitted = false;

  noteRessourceList!: NoteRessourceModel[];
  noteRessourceSubscription!: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private ressourceService: RessourceService,
    public sanitizer: DomSanitizer,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    if (localStorage.getItem("currentAgent") === null){
      this.isAuth! = false;
    } else {
      if (localStorage.getItem("role")=== "ADMIN"){
        this.role! = "ADMIN";
      } else {
        this.role! = "USER";
      }
      this.isAuth! = true;
    }

    let idRessource = this.route.snapshot.params.idRessource;
    this.ressourceService.getNoteRessourceByIdRessource(idRessource);

    this.ressourceService.getRessourceById(idRessource).subscribe(
      (ressource) => {
        this.ressource = ressource;
        this.URLFile = this.sanitizer.bypassSecurityTrustResourceUrl(ressource.lien);
      }
    )
    

    this.noteRessourceForm = this.formBuilder.group({
      titre: ['', Validators.required],
      note: ['', Validators.required]
    })

    this.noteRessourceSubscription = this.ressourceService.noteRessourceListSubject.subscribe(
      noteRessources => this.noteRessourceList = noteRessources
    );
  }

  get f() {
    return this.noteRessourceForm.controls;
  }

  onSubmit() {

    this.submitted = true;

    if (this.noteRessourceForm.invalid) {
      return;
    }

    let idRessource = this.route.snapshot.params['idRessource'];
    this.ressourceService.createNoteRessource(this.noteRessourceForm.value, idRessource);
  }

  deleteNoteRessource(noteRessource: NoteRessourceModel){
    let idRessource = this.route.snapshot.params['idRessource'];
    
    this.ressourceService.deleteNoteRessource(noteRessource.id).subscribe(
      () => this.ressourceService.getNoteRessourceByIdRessource(idRessource)
      , error => console.error(error.error.message)
    );
  }

  showForm(){
    if (this.isShow){
      this.isShow = false;
    } else {
      this.isShow = true;
    }
  }

  ngOnDestroy():void {
    this.noteRessourceSubscription.unsubscribe();

  }

}
