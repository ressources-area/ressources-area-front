import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RessourceModel} from '../../../models/Ressource.model';
import {RessourceService} from '../../../services/ressource.service';
import {DOCUMENT} from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-ressource-module',
  templateUrl: './list-ressource-module.component.html',
  styleUrls: ['./list-ressource-module.component.scss']
})
export class ListRessourceModuleComponent implements OnInit, OnDestroy {

  ressourceList!: RessourceModel[];
  ressourceListSubscription!: Subscription;

  isAuth!: boolean;
  role!: string;
  currentModule!: string;
  message!: string;

  constructor(private ressourceService: RessourceService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.ressourceListSubscription = this.ressourceService.ressourceListSubject.subscribe(
      ressources => this.ressourceList = ressources
    );

    if (localStorage.getItem("currentAgent") === null){
      this.isAuth! = false;
    } else {
      if (localStorage.getItem("role")=== "ADMIN"){
        this.role! = "ADMIN";
      } else {
        this.role! = "USER";
      }
      this.isAuth! = true;
    }


    (this.route).params.subscribe( params => {
      let module = this.route.snapshot.params['module'];

    this.currentModule = module;
    this.ressourceService.getRessourceListByModule(module);
    });
    
  }

  selectRessource(ressource: RessourceModel){
    let link = ['/ressources/ressource/'+ressource.id];
    this.router.navigate(link);
  }

  selectOtherRessource(otherRessource: string){
    this.ressourceService.getRessourceListByModule(otherRessource);
  }

  deleteRessource(ressource: RessourceModel){
    let module = ressource.module;
    this.ressourceService.deleteRessource(ressource).subscribe(
      () => this.ressourceService.getRessourceListByModule(module)
      , error => console.error(error.error.message)
    )

  }

  ngOnDestroy():void {
    this.ressourceListSubscription.unsubscribe();
  }
}
