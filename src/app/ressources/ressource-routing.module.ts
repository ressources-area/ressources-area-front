import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GuardService} from '../services/guard.service';
import {ListRessourceModuleComponent} from './ressource-list/list-ressource-module/list-ressource-module.component';
import {RessourceInfoComponent} from './ressource-info/ressource-info/ressource-info.component';
import {AdminGuardService} from '../services/admin-guard.service';
import {AddRessourceComponent} from './ressource-add/add-ressource/add-ressource.component';
import {UpdateRessourceComponent} from './ressource-update/update-ressource/update-ressource.component';

const ressourceRoutes: Routes = [
  {
    path: 'ressources',
    canActivate: [GuardService],
    children: [
      {path: 'listRessource/:module', component: ListRessourceModuleComponent},
      {path: 'ressource/:idRessource', component: RessourceInfoComponent}
    ]
  },
  {
    path: 'ressources',
    canActivate: [AdminGuardService],
    children: [
      {path: 'addRessource', component: AddRessourceComponent},
      {path: 'updateRessource/:idRessource', component: UpdateRessourceComponent},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ressourceRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class RessourceRoutingModule {}
