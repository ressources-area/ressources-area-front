import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RessourceRoutingModule} from './ressource-routing.module';
import {UpdateRessourceComponent} from './ressource-update/update-ressource/update-ressource.component';
import {ListRessourceModuleComponent} from './ressource-list/list-ressource-module/list-ressource-module.component';
import {RessourceInfoComponent} from './ressource-info/ressource-info/ressource-info.component';
import {AddRessourceComponent} from './ressource-add/add-ressource/add-ressource.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RessourceService} from '../services/ressource.service';

@NgModule({
    imports: [
        CommonModule,
        RessourceRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ],
  declarations: [
    UpdateRessourceComponent,
    ListRessourceModuleComponent,
    RessourceInfoComponent,
    AddRessourceComponent
  ],
  providers: [
    RessourceService
  ]
})

export class RessourceModule { }
