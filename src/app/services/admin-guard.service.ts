import {Injectable, OnInit} from '@angular/core';
import {AuthService} from './auth.service';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AgentModel} from '../models/Agent.model';
import {AgentService} from './agent.service';
import {first} from 'rxjs/operators';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate, OnInit {

  role!: string;

  constructor(private authService: AuthService,
              private agentService: AgentService,
              private tokenService: TokenService,
              private router: Router) {
  }

  ngOnInit(): void {
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url: string = state.url;
    const currentAgent = this.authService.currentAgentValue;
    if (currentAgent) {
      if (this.checkRole() === "ADMIN") {
        return true;
      } else {
        this.authService.redirectUrl = url;
        this.router.navigate(['/agent/profil']);
      }
    } else {
      
      this.router.navigate(['/auth/signin']);
      return false;
    }
    return false;
  }


  checkRole(): string {
    if (localStorage.getItem("role") === "ADMIN") {
      this.role! = "ADMIN";
    } else {
      this.role! = "USER";
    }
    return this.role;
  }
}
