import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AgentGuardService implements CanActivate{

  matricule!: string;

  constructor(private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    let url: string = state.url;
    //TODO Get tout derrière le dernier '/'
    let urlArray = url.split('/');
    this.matricule = urlArray[urlArray.length-1];
    if (this.matricule === localStorage.getItem('currentMatricule')){
      return true;
    }
    return true;
  }

}
