import {Injectable} from '@angular/core';
import {AgentModel} from '../models/Agent.model';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {CompetenceAgentModel} from '../models/CompetenceAgent.model';
import {CentreModel} from '../models/Centre.model';
import {GradeModel} from '../models/Grade.model';
import {CompetenceModel} from '../models/Competence.model';
import {SummaryAgentModel} from '../models/SummaryAgent.model';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  private baseUrl = "http://localhost:8181";

  agentProfil!: AgentModel;
  agentSubject = new Subject<AgentModel>();

  agentList!: AgentModel[];
  agentListSubject = new Subject<AgentModel[]>();

  private log(log:string){
    console.info(log);
  }

  private handleError<T>(operation='operation', result?:T){
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(operation+ 'failed:'+ error.message);

      return of(result as T );
    }
  }

  emitAgent() {
    this.agentSubject.next(this.agentProfil);
  }

  emitAgentList(){
    this.agentListSubject.next(this.agentList);
  }

  constructor(private http: HttpClient) {
  }

  // ----- Agent ----- //
  getProfilByMatriculeAgent(matricule: string) {
    return this.http.get<AgentModel>(this.baseUrl+'/agent/'+'profil/'+matricule, {withCredentials: true});
  }

  getAgentList(){
    this.http.get<AgentModel[]>(this.baseUrl+'/agent/listAgent',{withCredentials:true}).subscribe(
      (agents) => {
        this.agentList = agents;
        this.emitAgentList();
      }
    )
  }

  getProfilByAgentConnected(): Observable<AgentModel>{
    const url = this.baseUrl+'/auth/me';

    return this.http.get<AgentModel>(url,{withCredentials:true}).pipe(
      tap(_=>this.log("Récupération de l'agent connecté")),
      catchError(this.handleError<AgentModel>('AgentConnected'))
    );
  }

  getAgentNotInStage(matriculeList: string[]){

    return this.http.get<AgentModel[]>(this.baseUrl+'/agent/getAgentListInNotStage?matriculeList='+matriculeList,
        {withCredentials:true}).subscribe(
          (agents) => {
            this.agentList = agents;
            this.emitAgentList();
          }
        )
  }

  updateAgent(agentModel: AgentModel): Observable<AgentModel>{
    const httpOption = {
      headers: new HttpHeaders({'Content-type':'application/json'}),
      withCredentials:true
    };

    return this.http.put<AgentModel>(this.baseUrl+'/agent/update',agentModel, httpOption).pipe(
      tap(_=> this.log("Modification de l'agent "+ agentModel.nom)),
      catchError(this.handleError<any>('updateAgent'))
    );
  }

  updateRoleAgent(agentModel: AgentModel){
    this.http.put(this.baseUrl+'/agent/changeRole', agentModel, {withCredentials: true})
      .subscribe();
  }

  deleteAgent(agentModel: AgentModel){
    this.http.delete(this.baseUrl+'/agent/delete/'+agentModel.id, {withCredentials: true})
      .subscribe(
        () => {
          this.getAgentList();
          this.emitAgentList();
        }
      );
  }

  updatePassword(agentModel: AgentModel, newPassword: string, oldPassword: string){
    const params = new HttpParams()
      .set('oldPassword', newPassword)
      .set('newPassword', oldPassword);
    this.http.put(this.baseUrl+'/agent/updatePassword', agentModel,{params: params,withCredentials: true})
      .subscribe()}


  searchAgent(term: string): Observable<SummaryAgentModel[]> {
    if (!term.trim()){
      return of([]);
    }
    return this.http.get<SummaryAgentModel[]>(this.baseUrl+'/agent/searchAgent?nomPrenom='+term, {withCredentials:true});
  }
  // ----- Centre ----- //

  getAllCentre(): Observable<CentreModel[]>{
    return this.http.get<CentreModel[]>(this.baseUrl+'/centre/getAllCentre', {withCredentials: true});
  }
  // ----- Grade ----- //

  getAllGrade(): Observable<GradeModel[]>{
    return this.http.get<GradeModel[]>(this.baseUrl+'/grade/getAllGrade', {withCredentials: true});
  }
  // ----- Compétence ----- //

  getAllCompetence(): Observable<CompetenceModel[]> {
    return this.http.get<CompetenceModel[]>(this.baseUrl+'/competence/getAllCompetence', {withCredentials: true})
  }
  // ----- CompétenceAgent ----- //

  getCompetenceAgentByMatricule(matricule: string): Observable<CompetenceAgentModel[]>{
    return this.http.get<CompetenceAgentModel[]>(this.baseUrl+'/competenceAgent/'+matricule, {withCredentials: true});
  }

  updateCompetenceAgent(competenceAgentList: CompetenceAgentModel[], agent: AgentModel){
    return this.http.put(this.baseUrl+'/competenceAgent/update/'+agent.matricule, competenceAgentList, {withCredentials: true})
  }

}
