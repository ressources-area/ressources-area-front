import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthModel} from '../models/Auth.model';
import {map} from 'rxjs/operators';
import {AgentModel} from '../models/Agent.model';
import {AgentService} from './agent.service';
import {Router} from '@angular/router';
import {TokenService} from './token.service';
import { getRtlScrollAxisType } from '@angular/cdk/platform';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentAgentSubject: BehaviorSubject<AuthModel>;
  public currentAgent!: Observable<AuthModel>;
  authModelLogin!: AuthModel;
  redirectUrl!: string;

  private role = new BehaviorSubject<string>("");
  currentRole = this.role.asObservable();

  private isAuth = new BehaviorSubject<boolean>(false);
  currentAuth = this.isAuth.asObservable();

  constructor(private http: HttpClient,
              private agentService: AgentService,
              private router: Router,
              private tokenService: TokenService) {
    this.currentAgentSubject = new BehaviorSubject<AuthModel>(
      JSON.parse(<string> localStorage.getItem('currentAgent')));
    this.currentAgent = this.currentAgentSubject.asObservable();

  }

  public get currentAgentValue(): AuthModel {
    return this.currentAgentSubject.value;
  }

  getRole(role:string){  
    this.role.next(role);
  }

  getisAuth(isAuth:boolean){
    this.isAuth.next(isAuth);
  }

  signIn(authModelLogin: AuthModel){
    console.log("Authentication en cours");
    // @ts-ignore
    return this.http.post<AuthModel>('http://localhost:8181/auth/login', authModelLogin,{withCredentials: true})
      .pipe(map(authModel => {
        localStorage.setItem('currentAgent',JSON.stringify(authModel));
        let storage = JSON.stringify(localStorage.getItem("currentAgent"));
        let arrayStorage = storage.split(',');
        let token = arrayStorage[3];
        token = this.tokenService.getToken(token);
        let currentMatricule = this.tokenService.getMatriculeInToken(token);
        let role = this.tokenService.getRole(token);
        localStorage.setItem('currentMatricule', currentMatricule);
        localStorage.setItem('role', role);
        this.currentAgentSubject.next(authModel);
        this.getRole(role);
        this.getisAuth(true);
      }));
  }

  registrer(agentModel: AgentModel){
    return this.http.post('http://localhost:8181/agent/create', agentModel, {withCredentials: true});
  }

  signout(){
    this.http.get('http://localhost:8181/auth/logout',{withCredentials: true})
      .subscribe(
        data => {
          console.log("Déconnexion");
        }
      );
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentAgent');
    localStorage.removeItem('role');
    localStorage.removeItem('currentMatricule')
    // @ts-ignore
    this.currentAgentSubject.next(null);
  }
}
