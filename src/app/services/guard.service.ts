import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService  implements CanActivate{

  constructor(private authService: AuthService,
              private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let url: string = state.url;
    return this.checkLogin(url);
  }

   checkLogin(url: string): boolean {

    if(this.authService.currentAgentValue){
      return true;
    }
    this.authService.redirectUrl = url;
    this.router.navigate(['/auth/signin']);

    return false;
   }
}
