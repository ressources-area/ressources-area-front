import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AgentModule} from '../agent/agent.module';
import {AgentModel} from '../models/Agent.model';
import {AgentService} from './agent.service';
import {AuthService} from './auth.service';
import {StageService} from './stage.service';
import {StageModel} from '../models/Stage.model';

@Injectable({
  providedIn: 'root'
})
export class InfoStageGuardService implements CanActivate {

  agentModel!: AgentModel;
  stageModel!: StageModel;

  constructor(private route: ActivatedRoute,
              private agentService: AgentService,
              private authService: AuthService,
              private stageService: StageService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
      return true;
  }

  checkAccessStageSuccess(matricule: string, url: string): boolean {
    return true;
  }
}
