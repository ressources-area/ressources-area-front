import {Injectable} from '@angular/core';
import {RessourceModel} from '../models/Ressource.model';
import {Observable, of, Subject} from 'rxjs';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {NoteRessourceModel} from '../models/NoteRessource.model';


@Injectable({
  providedIn: 'root'
})
export class RessourceService {

  private baseUrl = "http://localhost:8282";

  ressource!: RessourceModel;
  ressourceSubject = new Subject<RessourceModel>();
  
  ressourceList!: RessourceModel[];
  ressourceListSubject = new Subject<RessourceModel[]>();

  noteRessourceList!: NoteRessourceModel[];
  noteRessourceListSubject = new Subject<NoteRessourceModel[]>();

  private log(log:string){
    console.info(log);
  }

  private handleError<T>(operation='operation', result?:T){
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(operation+ 'failed:'+ error.message);

      return of(result as T );
    }
  }

  constructor(private http: HttpClient) { }

  emitRessource() {
    this.ressourceSubject.next(this.ressource);
  }

  emitRessourceList() {
    this.ressourceListSubject.next(this.ressourceList);
  }

  emitNoteRessourceList() {
    this.noteRessourceListSubject.next(this.noteRessourceList);
    console.log(this.noteRessourceList);
  }

  createRessource(ressource: RessourceModel){

    return this.http.post(this.baseUrl+'/ressource/create',ressource,
      {withCredentials:true, reportProgress:true, responseType:'json'});

  }

  upload(file: File, module: string): Observable<HttpEvent<any>>{

    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', this.baseUrl+'/file/upload/'+module, formData,
      {withCredentials: true});
    return this.http.request(req);

  }

  getFiles(): Observable<any> {
    return this.http.get(this.baseUrl+'files');
  }

  getRessourceById(idRessource: string){
    return this.http.get<RessourceModel>(this.baseUrl+'/ressource/ressourceInfo/'+idRessource,
      {withCredentials: true});
  }

  getFileByFileName(titre: string){
    const httpOption = {
      withCredentials:true,
      'responseType': 'arraybuffer' as 'json'
    };

    return this.http.get<any>(this.baseUrl+'/file/download/'+titre,httpOption);
  }

  deleteRessource(ressource: RessourceModel){
    return this.http.delete(this.baseUrl+'/ressource/delete/'+ressource.id, {withCredentials:true});
  }

  getRessourceListByModule(module: string){
    this.http.get<RessourceModel[]>(this.baseUrl+'/ressource/ressourceByModule/'+module, {withCredentials:true})
    .subscribe(
      (ressources) => {
        this.ressourceList = ressources;
        this.emitRessourceList();
      }
    )
  }

  // ----- Note Ressource ----- //

  createNoteRessource(noteRessource: NoteRessourceModel, idRessource: number){

    this.http.post(this.baseUrl+'/noteRessource/create/'+idRessource, noteRessource,
      {withCredentials:true}).subscribe(
        () => {
          console.log("Création d'une note ressource ok !");
          this.getNoteRessourceByIdRessource(idRessource);
        },
        (err) => {
          console.log("Erreur de création d'une note ressource");
        }
      )
  }

  getNoteRessourceByIdRessource(idRessource: number){
    this.http.get<NoteRessourceModel[]>(this.baseUrl+'/noteRessource/ressource/'+idRessource, {withCredentials:true})
    .subscribe(
        (noteRessources) => {
          this.noteRessourceList = noteRessources;
          this.emitNoteRessourceList();
        },
        (err)=> {
          console.log(err.error.message);
        }
      )
  }

  deleteNoteRessource(idNoteRessource: number){
    return this.http.delete(this.baseUrl+'/noteRessource/delete/'+idNoteRessource,{withCredentials:true});
  }
}
