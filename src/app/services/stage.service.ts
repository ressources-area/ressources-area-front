import { Injectable } from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {StageModel} from '../models/Stage.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AgentStageModel} from '../models/AgentStage.model';
import { NoteStageModel } from '../models/NoteStage.model';
import {catchError, tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StageService {

  private baseUrl = "http://localhost:8383";

  matriculeList!: string[];
  matriculeListSubject = new Subject<string[]>();

  stage!: StageModel;
  stageSubject = new Subject<StageModel>();

  stageList!: StageModel[];
  stageListSubject = new Subject<StageModel[]>();

  agentStageList!: AgentStageModel[];
  agentStageListSubject = new Subject<AgentStageModel[]>();

  noteStageList!: NoteStageModel[];
  noteStageListSubject = new Subject<NoteStageModel[]>();


  private log(log:string){
    console.info(log);
  }

  private handleError<T>(operation='operation', result?:T){
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(operation+ 'failed:'+ error.message);

      return of(result as T );
    }
  }

  constructor(private http: HttpClient) { }

  emitMatriculeList(){
    this.matriculeListSubject.next(this.matriculeList);
  }
  
  emitStage(){
    this.stageSubject.next(this.stage);
  }

  emitNoteStageList(){
    this.noteStageListSubject.next(this.noteStageList);
  }

  emitAgentStageList(){
    this.agentStageListSubject.next(this.agentStageList);
  }

  emitStageList(){
    this.stageListSubject.next(this.stageList);
  }


// Method AgentStage

  addAgentStage(agentStage: AgentStageModel, idStage: number){
    return this.http.post(this.baseUrl+'/agentStage/create/'+idStage, agentStage, {withCredentials:true});
  }

  getAgentStageList(idStage: number){
    return this.http.get<AgentStageModel[]>(this.baseUrl+'/agentStage/stage/'+idStage,{withCredentials:true}).subscribe(
      (agentStages) => {
        this.agentStageList = agentStages;
        this.emitAgentStageList();
      }
    );
  }

  getListMatriculeInStage(idStage: number){
    return this.http.get<string[]>(this.baseUrl+'/agentStage/getListMatriculeInStage/'+idStage,{withCredentials:true})
    .subscribe(
      matricules => {
        this.matriculeList = matricules;
        this.emitMatriculeList();
      }
    )
  }

  deleteAgentStage(idAgentStage: number){
    return this.http.delete(this.baseUrl+'/agentStage/delete/'+idAgentStage, {withCredentials:true});
  }

// Method Stage

  getStageById(idStage: number){
    this.http.get<StageModel>(this.baseUrl+'/stage/'+idStage,{withCredentials:true}).subscribe(
      stage => {
        this.stage = stage;
        this.emitStage();
      }
    );
  }

  getStageList() {
    return this.http.get<StageModel[]>(this.baseUrl+'/stage/stageList',{withCredentials:true}).subscribe(
      (stages) => {
        this.stageList = stages;
        this.emitStageList();
      }
    );
  }

  getStageByMatriculeAgent(matricule: string): Observable<StageModel[]>{
    return this.http.get<StageModel[]>(this.baseUrl+'/stage/getListStageByAgent/'+matricule, {withCredentials:true})
  }

  deleteStage(idStage: number){
    this.http.delete(this.baseUrl+'/stage/delete/'+idStage,{withCredentials:true}).subscribe(
      () => { 
        this.getStageList();
        this.emitStageList();
      }
    );
  }

  addStage(stage: StageModel){
    return this.http.post<StageModel>(this.baseUrl+'/stage/create',stage, {withCredentials:true});
  }

  updateStage(stage: StageModel): Observable<StageModel>{
    const httpOption = {
      headers: new HttpHeaders({'Content-type':'application/json'}),
      withCredentials:true
    };

    return this.http.put<StageModel>(this.baseUrl+'/stage/update',stage, httpOption).pipe(
      tap(_=> this.log("Modification de du stage "+ stage.intitule)),
      catchError(this.handleError<any>('updateStage'))
    );
  }

// Method Note Stage

  getNoteStageById(idStage: number){
    this.http.get<NoteStageModel[]>(this.baseUrl+'/noteStage/stage/note/'+idStage, {withCredentials: true})
    .subscribe(
      (noteList) => {
        this.noteStageList = noteList;
        this.emitNoteStageList();
      },
      (err) => {
        console.log(err.error.message);
      }
    )
    
  }

  addNoteStage(noteStage: NoteStageModel, idStage: number): Observable<NoteStageModel[]>{
   return this.http.post<NoteStageModel[]>(this.baseUrl+'/noteStage/create/'+idStage, noteStage, {withCredentials: true});
    
  }

  deleteNoteStage(idNoteStage: number){
    return this.http.delete(this.baseUrl+'/noteStage/delete/'+idNoteStage,{withCredentials:true});
  }
}


