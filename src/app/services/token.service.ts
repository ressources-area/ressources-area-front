import {Injectable} from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders, HttpRequest} from '@angular/common/http';
import {CookieOptionsProvider, CookieService} from 'ngx-cookie';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  jwtToken!: string;
  decodeToken!: { [key: string]: string };

  constructor() {
  }

  httpHeader = new HttpHeaders();


  getMatriculeInToken(token: string): string {
    let matricule = JSON.parse(atob(token.split('.')[1]));
    matricule = matricule.sub;
    return matricule;
  }

  getToken(token: string): string {
    token = token.substring(12, token.length-3);
    return token;
  }

  getRole(token:string): string {
    let role = JSON.parse(atob(token.split('.')[1]));
    role = role.role;
    return role;
  }
}
