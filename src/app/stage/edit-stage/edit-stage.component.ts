import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StageModel } from 'src/app/models/Stage.model';
import { StageService } from 'src/app/services/stage.service';

@Component({
  selector: 'app-edit-stage',
  templateUrl: './edit-stage.component.html',
  styleUrls: ['./edit-stage.component.scss']
})
export class EditStageComponent implements OnInit {

  @Input() stage!: StageModel;
  stageSubscription!: Subscription;

  constructor(private router: Router,
              private stageService: StageService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    let idStage = this.route.snapshot.params.idStage;
    this.stageSubscription = this.stageService.stageSubject.subscribe(
      stage => this.stage = stage
    );

    this.stageService.getStageById(idStage);
  }

  onSubmit(): void {
    console.log(this.stage);
    this.stageService.updateStage(this.stage)
    .subscribe(
      () => {
        this.goBack();
      },
      () => {
        this.goBack();
      }
    )
  }

  goBack(): void {
    let idStage = this.route.snapshot.params.idStage;
    let link = ['stage/info-stage/'+idStage];
    this.router.navigate(link);
  }

}
