import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CentreModel } from 'src/app/models/Centre.model';
import { AgentService } from 'src/app/services/agent.service';
import { StageService } from 'src/app/services/stage.service';

@Component({
  selector: 'app-add-stage',
  templateUrl: './add-stage.component.html',
  styleUrls: ['./add-stage.component.scss']
})
export class AddStageComponent implements OnInit {

  addStageForm!: FormGroup;
  loading = false;
  submitted = false;

  centreList!: CentreModel[];

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private stageService: StageService,
              private agentService: AgentService ) { }

  ngOnInit(): void {

    this.agentService.getAllCentre().subscribe(
      data => {
          this.centreList = data;
      }
    )

    this.addStageForm = this.formBuilder.group({
      intitule: ['',Validators.required],
      numero: ['',Validators.required],
      dateDebut: ['',Validators.required],
      dateFin: ['',Validators.required],
      lieu: ['',Validators.required]
    })
  }

  get f(){
    return this.addStageForm.controls;
  }

  onSubmit(){
    this.submitted = true;

    console.log(this.addStageForm.value);

    if(this.addStageForm.invalid){
      console.log("Invalid")
      return;
    }

    this.stageService.addStage(this.addStageForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.router.navigate(['/stage/list-stage']);
      },
      error => {
        this.router.navigate(['/stage/list-stage']);
      }
    )
  }

}
