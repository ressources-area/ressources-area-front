import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { AgentModel } from 'src/app/models/Agent.model';
import { AgentStageModel } from 'src/app/models/AgentStage.model';
import { NoteStageModel } from 'src/app/models/NoteStage.model';
import { StageModel } from 'src/app/models/Stage.model';
import { AgentService } from 'src/app/services/agent.service';
import { StageService } from 'src/app/services/stage.service';

@Component({
  selector: 'app-info-stage',
  templateUrl: './info-stage.component.html',
  styleUrls: ['./info-stage.component.scss']
})
export class InfoStageComponent implements OnInit, OnDestroy {

  stage!: StageModel;

  agentList!: AgentModel[];
  agentListSubscription!: Subscription;

  agentStageList!: AgentStageModel[];
  agentStageListSubscription!: Subscription;

  stageSubscription!: Subscription;

  noteStageList!: NoteStageModel[];
  noteStage!: NoteStageModel;
  noteStageListSubscription!: Subscription;

  matriculeList = new Array();
  matriculeSubscription!: Subscription;

  isAuth!: Boolean;
  role!: string;
  isShow!: Boolean;
  registerForm!: FormGroup;
  noteRegisterForm!: FormGroup;
  submitted = false;
  noteSubmitted = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private stageService: StageService,
              private formBuilder: FormBuilder,
              private agentService: AgentService) { }

  ngOnInit(): void {
    let idStage = this.route.snapshot.params.idStage;

    this.stageSubscription = this.stageService.stageSubject.subscribe(
      stage => this.stage = stage
    );

    this.noteStageListSubscription = this.stageService.noteStageListSubject.subscribe(
      notes => this.noteStageList = notes
    );

    this.agentStageListSubscription = this.stageService.agentStageListSubject.subscribe(
      agentStages => this.agentStageList = agentStages
    );

    this.agentListSubscription = this.agentService.agentListSubject.subscribe(
      agents => this.agentList = agents
    );

    this.matriculeSubscription = this.stageService.matriculeListSubject.subscribe(
      matricules => this.matriculeList = matricules
    );

    this.getStageById(idStage);
    this.getMatriculeAgentNotInStage(idStage)
    this.getAgentList(this.matriculeList);
    this.getAgentStageList(idStage);
    this.getNoteStage(idStage);

    if (localStorage.getItem("currentAgent") === null){
      this.isAuth! = false;
    } else {
      if (localStorage.getItem("role")=== "ADMIN"){
        this.role! = "ADMIN";
      } else {
        this.role! = "USER";
      }
      this.isAuth! = true;
    }

    this.registerForm = this.formBuilder.group({
      agent: ['',Validators.required],
      status: ['', Validators.required]
    });

    this.noteRegisterForm = this.formBuilder.group({
      titre: ['',Validators.required],
      note: ['',Validators.required]
    });
  }

  get f(){
    return this.registerForm.controls;
  }

  get fNote(){
    return this.noteRegisterForm.controls;
  }

  getMatriculeAgentNotInStage(idStage: number){
    this.stageService.getListMatriculeInStage(idStage);
  }

  getAgentList(matricules: string[]){
      this.agentService.getAgentNotInStage(matricules);
  }

  getAgentStageList(idStage: number){
    this.stageService.getAgentStageList(idStage);
  }

  getStageById(idStage: number){
    this.stageService.getStageById(idStage);
  }

  getNoteStage(idStage: number){
    this.stageService.getNoteStageById(idStage);


  }

  onSubmit(){
    this.submitted = true;

    if(this.registerForm.invalid){
      return;
    }

    let idStage = this.route.snapshot.params.idStage;
    let agentStage = new AgentStageModel();

    agentStage.matriculeAgent = this.registerForm.value.agent.matricule;
    agentStage.nomAgent = this.registerForm.value.agent.nom;
    agentStage.prenomAgent = this.registerForm.value.agent.prenom;
    agentStage.nomCentre = this.registerForm.value.agent.centre.nom;
    agentStage.status = this.registerForm.value.status;

    console.log(agentStage);

    this.stageService.addAgentStage(agentStage, idStage)
    .pipe(first())
    .subscribe(
      event => {
        window.location.reload();
      },
      err => {
        console.log("Erreur lors de l'ajout d'un agent");
      }
    );
  }

  deleteAgentStage(idAgentStage: number){
    let idStage = this.route.snapshot.params.idStage;

    this.stageService.deleteAgentStage(idAgentStage).subscribe(
      () => this.stageService.getAgentStageList(idStage)
      , error => console.error(error.error.message)
      );
  }

  onNoteSubmit(){
      this.noteSubmitted = true;

      let idStage = this.route.snapshot.params.idStage;

      this.stageService.addNoteStage(this.noteRegisterForm.value, idStage).subscribe(
        () => this.stageService.getNoteStageById(idStage)
        , error => console.error(error.error.message)
      );
  }

  deleteStage(stage: StageModel){
      this.stageService.deleteStage(stage.id);
      this.goBack();
  }

  updateStage(stage: StageModel){
    let link = ['stage/edit-stage/'+stage.id]
    this.router.navigate(link);
  }

  deleteNoteStage(noteStage: NoteStageModel){
    let idStage = this.route.snapshot.params.idStage;

    this.stageService.deleteNoteStage(noteStage.id).subscribe(
      () => this.stageService.getNoteStageById(idStage)
      , error => console.error(error.error.message)
    );
  }

  goBack(): void {
    let link = ['stage/list-stage'];
    this.router.navigate(link);
  }

  ngOnDestroy(){
    this.agentStageListSubscription.unsubscribe();
    this.noteStageListSubscription.unsubscribe();
    this.stageSubscription.unsubscribe();
    this.agentListSubscription.unsubscribe();
  }
}
