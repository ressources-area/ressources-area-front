import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StageModel } from 'src/app/models/Stage.model';
import { StageService } from 'src/app/services/stage.service';

@Component({
  selector: 'app-list-stage',
  templateUrl: './list-stage.component.html',
  styleUrls: ['./list-stage.component.scss']
})
export class ListStageComponent implements OnInit,OnDestroy {

  stageList!: StageModel[];
  stageListSubscription!: Subscription;

  role!: string;
  isAuth!: boolean;

  constructor(private stageService: StageService,
              private router: Router) { }

  ngOnInit(): void {
      this.stageListSubscription = this.stageService.stageListSubject.subscribe(
        stages => this.stageList = stages
      );

      this.listStage();

      if (localStorage.getItem("currentAgent") === null){
        this.isAuth! = false;
      } else {
        if (localStorage.getItem("role")=== "ADMIN"){
          this.role! = "ADMIN";
        } else {
          this.role! = "USER";
        }
        this.isAuth! = true;
      }
  }


  listStage(){
    this.stageService.getStageList();
  }

  selectStage(idStage: number){
    let link = ['/stage/info-stage/'+idStage];
    this.router.navigate(link);
  }

  ngOnDestroy(): void {
    this.stageListSubscription.unsubscribe();
  }

}
