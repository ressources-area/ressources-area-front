import {RouterModule, Routes} from '@angular/router';
import {AdminGuardService} from '../services/admin-guard.service';
import {AddStageComponent} from './stage-add/add-stage.component';
import {ListStageComponent} from './stage-list/list-stage.component';
import {InfoStageGuardService} from '../services/info-stage-guard.service';
import {NgModule} from '@angular/core';
import { InfoStageComponent } from './stage-info/info-stage.component';
import { EditStageComponent } from './edit-stage/edit-stage.component';

const stageRoutes: Routes = [
  {
    path:'stage',
    canActivate: [AdminGuardService],
    children : [
      { path: 'add-stage', component: AddStageComponent},
      { path: 'list-stage', component: ListStageComponent},
      { path: 'edit-stage/:idStage', component: EditStageComponent}
    ]
  },
  {
    path:'stage',
    canActivate: [InfoStageGuardService],
    children : [
      { path: 'info-stage/:idStage', component: InfoStageComponent}
    ]
  }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(stageRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class StageRoutingModule{}
