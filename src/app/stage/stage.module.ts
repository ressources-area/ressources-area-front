import {NgModule} from '@angular/core';
import {StageService} from '../services/stage.service';
import {AdminGuardService} from '../services/admin-guard.service';
import {InfoStageGuardService} from '../services/info-stage-guard.service';
import {AddStageComponent} from './stage-add/add-stage.component';
import {ListStageComponent} from './stage-list/list-stage.component';
import {InfoStageComponent} from './stage-info/info-stage.component';
import {StageRoutingModule} from './stage-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EditStageComponent } from './edit-stage/edit-stage.component';

@NgModule({
  imports: [
    StageRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AddStageComponent,
    ListStageComponent,
    InfoStageComponent,
    EditStageComponent,
  ],
  providers: [
    StageService,
    AdminGuardService,
    InfoStageGuardService
  ]
})
export class StageModule { }
